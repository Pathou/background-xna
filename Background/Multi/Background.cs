using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Backgrounds
{
	public class Background
	{
		protected Texture2D _texture;
		protected Point _screen;
		protected Point _size;

		protected Vector2 _pos;
		protected Color _color;

		public Vector2 Pos { get { return _pos; } set { _pos = value; } }
		public Color Color { get { return _color; } set { _color = value; } }
		public Point Size { get { return _size; } }

		public Background(Texture2D texture)
		{
			_texture = texture;

			this.Init();
		}
		public Background(Texture2D texture, Vector2 pos)
		{
			_texture = texture;
			_pos = pos;

			this.Init();
		}

		#region XNA

		protected void Init()
		{
			Color = Color.White;
			_size = new Point(_texture.Width, _texture.Height);
		}

		public void Update(GameTime gameTime)
		{
		}

		public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
		{
			spriteBatch.Draw(_texture, _pos, Color);
		}

		#endregion

		#region M�thode

		#endregion
	}
}
