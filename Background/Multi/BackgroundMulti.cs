using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Backgrounds
{
	public class BackgroundMulti
	{
		public enum MoveDirection { Top, Right, Bottom, Left }

		protected Point _screen;

		protected MoveDirection _direction;
		protected float _speed;

		protected Queue<Background> _bgs;
		protected Background[] _currentBg;

		public float Speed { get { return _speed; } set { _speed = value; } }

		public BackgroundMulti(Queue<Background> bgs, Point screen, MoveDirection direction, float speed = 0.1f)
		{
			_bgs = bgs;
			_screen = screen;
			_direction = direction;
			_speed = speed;

			this.Init();
		}

		#region XNA

		protected void Init()
		{
			if(_bgs.Count > 1)
			{
				Background bg1 = _bgs.Dequeue();
				Background bg2 = _bgs.Dequeue();

				_currentBg = new Background[] { bg1, bg2 };

				// Initialisation des positions
				_currentBg[0].Pos = Vector2.Zero;
				switch(_direction)
				{
					case MoveDirection.Top:
						_currentBg[1].Pos = new Vector2(0, _currentBg[0].Size.Y);
						break;
					case MoveDirection.Bottom:
						_currentBg[1].Pos = new Vector2(0, -_currentBg[0].Size.Y);
						break;
					case MoveDirection.Left:
						_currentBg[1].Pos = new Vector2(_currentBg[0].Size.X, 0);
						break;
					default: // Right
						_currentBg[1].Pos = new Vector2(-_currentBg[0].Size.X, 0);
						break;
				}
			}
		}

		public void Update(GameTime gameTime)
		{
			this.MoveBackground(gameTime);
		}

		public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
		{
			foreach (Background bg in _currentBg)
			{
				bg.Draw(spriteBatch, gameTime);
			}
		}

		#endregion

		#region M�thode

		/// <summary>
		/// Bouge le background
		/// </summary>
		/// <param name="gameTime"></param>
		protected void MoveBackground(GameTime gameTime)
		{
			if (Speed != 0)
			{
				Vector2 move;
				switch (_direction)
				{
					case MoveDirection.Top:
						move = new Vector2(0, -1) * Speed * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
						_currentBg[0].Pos += move;
						_currentBg[1].Pos += move;
						break;
					case MoveDirection.Bottom:
						move = new Vector2(0, 1) * Speed * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
						_currentBg[0].Pos += move;
						_currentBg[1].Pos += move;
						break;
					case MoveDirection.Left:
						move = new Vector2(-1, 0) * Speed * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
						_currentBg[0].Pos += move;
						_currentBg[1].Pos += move;
						break;
					default: // Right
						move = new Vector2(1, 0) * Speed * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
						_currentBg[0].Pos += move;
						_currentBg[1].Pos += move;
						break;
				}

				this.CheckIfIsOut(); // Si en dehors on r�initialise
			}
		}


		/// <summary>
		/// V�rifie si le background a fini sa loop et r�initilise le cas �ch�ant
		/// </summary>
		protected void CheckIfIsOut()
		{
			switch(_direction)
			{
				case MoveDirection.Top:
					if (_currentBg[0].Pos.Y <=  -_screen.Y)
					{
						_bgs.Enqueue(_currentBg[0]);
						_currentBg[0] = _currentBg[1];
						_currentBg[1] = _bgs.Dequeue();
						_currentBg[1].Pos = new Vector2(0, _currentBg[1].Size.Y + _currentBg[0].Pos.Y);
					}
					break;
				case MoveDirection.Bottom:
					if (_currentBg[0].Pos.Y >= _screen.Y)
					{
						_bgs.Enqueue(_currentBg[0]);
						_currentBg[0] = _currentBg[1];
						_currentBg[1] = _bgs.Dequeue();
						_currentBg[1].Pos = new Vector2(0, -_currentBg[1].Size.Y + _currentBg[0].Pos.Y);
					}
					break;
				case MoveDirection.Left:
					if (_currentBg[0].Pos.X <= -_screen.X)
					{
						_bgs.Enqueue(_currentBg[0]);
						_currentBg[0] = _currentBg[1];
						_currentBg[1] = _bgs.Dequeue();
						_currentBg[1].Pos = new Vector2(_currentBg[1].Size.X + _currentBg[0].Pos.X, 0);
					}
					break;
				default: // Right
					if (_currentBg[0].Pos.X >= _screen.X)
					{
						_bgs.Enqueue(_currentBg[0]);
						_currentBg[0] = _currentBg[1];
						_currentBg[1] = _bgs.Dequeue();
						_currentBg[1].Pos = new Vector2(-_currentBg[1].Size.X + _currentBg[0].Pos.X, 0);
					}
					break;
			}
		}

		#endregion
	}
}
