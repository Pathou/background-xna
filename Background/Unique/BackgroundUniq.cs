using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Backgrounds
{
	public class BackgroundUniq
	{
		public enum MoveDirection { Top, Right, Bottom, Left }

		protected Texture2D _texture;
		protected Point _screen;
		protected int _width;
		protected int _height;

		protected Vector2 _pos;
		protected Color _color;
		protected MoveDirection _direction;
		protected Vector2 _deplacement;
		protected float _speed;
		protected Rectangle _destination;

		public Color Color { get { return _color; } set { _color = value; } }
		public float Speed { get { return _speed; } set { _speed = value; } }

		public BackgroundUniq(Texture2D texture, Point screen, MoveDirection direction, float speed = 0.1f)
		{
			_texture = texture;
			_screen = screen;
			_direction = direction;
			_speed = speed;

			this.Init();
		}

		#region XNA

		protected void Init()
		{
			Color = Color.White;
			_width = _texture.Width;
			_height = _texture.Height;

			this.InitDestination();
		}

		public void Update(GameTime gameTime)
		{
			this.MoveBackground(gameTime);
		}

		public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
		{
			spriteBatch.Draw(_texture, _pos, _destination, Color);
			//spriteBatch.Draw(_texture, _pos, _destination, Color, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
		}

		#endregion

		#region M�thode

		/// <summary>
		/// Bouge le background
		/// </summary>
		/// <param name="gameTime"></param>
		protected void MoveBackground(GameTime gameTime)
		{
			if (Speed != 0)
			{

				switch(_direction)
				{
					case MoveDirection.Top:
						_deplacement += new Vector2(0, -1) * Speed * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
						break;
					case MoveDirection.Bottom:
						_deplacement += new Vector2(0, 1) * Speed * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
						break;
					case MoveDirection.Left:
						_deplacement += new Vector2(-1, 0) * Speed * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
						break;
					default: // Right
						_deplacement += new Vector2(1, 0) * Speed * (float)gameTime.ElapsedGameTime.TotalMilliseconds;
						break;
				}

				_destination.X = (int)_deplacement.X;
				_destination.Y = (int)_deplacement.Y;

				this.CheckIfIsOut(); // Si en dehors on r�initialise
			}
		}

		/// <summary>
		/// Initialise la destination de d�part
		/// </summary>
		protected void InitDestination()
		{
			if (_direction == MoveDirection.Bottom || _direction == MoveDirection.Right)
			{
				_destination = new Rectangle(0, 0, _screen.X, _screen.Y);
			}
			else if (_direction == MoveDirection.Top)
			{
				_destination = new Rectangle(0, _height - _screen.Y, _screen.X, _screen.Y);
			}
			else // left
			{
				_destination = new Rectangle(_width - _screen.X, 0, _screen.X, _screen.Y);
			}

			_deplacement = new Vector2(_destination.X, _destination.Y);
		}


		/// <summary>
		/// V�rifie si le background a fini sa loop et r�initilise le cas �ch�ant
		/// </summary>
		protected void CheckIfIsOut()
		{
			switch(_direction)
			{
				case MoveDirection.Top:
					if (_destination.Y <= 0)
					{
						_destination = new Rectangle(0, _height - _screen.Y + _destination.Y, _screen.X, _screen.Y);
						_deplacement = new Vector2(_destination.X, _destination.Y);
					}
					break;
				case MoveDirection.Bottom:
					if (_destination.Y >= _height - _screen.Y)
					{
						_destination = new Rectangle(0, _screen.Y - _destination.Y, _screen.X, _screen.Y);
						_deplacement = new Vector2(_destination.X, _destination.Y);
					}
					break;
				case MoveDirection.Left:
					if (_destination.X <= 0)
					{
						_destination = new Rectangle(_width - _screen.X + _destination.X, 0, _screen.X, _screen.Y);
						_deplacement = new Vector2(_destination.X, _destination.Y);
					}
					break;
				default: // Right
					if (_destination.X >= _width - _screen.X)
					{
						_destination = new Rectangle(_destination.X + _screen.X - _width, 0, _screen.X, _screen.Y);
						_deplacement = new Vector2(_destination.X, _destination.Y);
					}
					break;
			}
		}

		#endregion
	}
}
